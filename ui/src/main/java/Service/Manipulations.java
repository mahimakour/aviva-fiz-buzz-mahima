package Service;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Manipulations {

	 int number;
	
	 String day;
	

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	
	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}
	
	
	
}
