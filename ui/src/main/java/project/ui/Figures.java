package project.ui;

import java.util.ArrayList;

import javax.print.attribute.standard.Media;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.annotation.XmlRootElement;

import Service.Manipulations;
import Service.Numeric;



@Path("/calculations")
@Produces(MediaType.APPLICATION_XML)
@Consumes(MediaType.TEXT_PLAIN)
public class Figures {
	Manipulations m=new Manipulations();
	Numeric n=new Numeric();
	
	
	@POST
	public int addNumber(int number) throws Exception{
		if(number<1||number>100){
			throw new Exception("Invalid Number");
		}
		else{
			m.setNumber(5);
			return number;
		}
		
	}
	
	
	@GET
	public ArrayList displayDigits(){
		int s=m.getNumber();
		ArrayList list=new ArrayList();
		
		for(int i=1;i<=s;i++){
			list.add(i);
		}
		return list;
		
	}
	
	@GET
	public void dayWise(@QueryParam("day") String day){
		 n.checkDay(day);
	}
	
	
	
	
	
	
	
}
