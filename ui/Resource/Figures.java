package Resource;

import javax.print.attribute.standard.Media;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.annotation.XmlRootElement;

import Service.Manipulations;

@XmlRootElement

@Path("/calculations")
@Produces(MediaType.APPLICATION_XML)
@Consumes(MediaType.TEXT_PLAIN)
public class Figures {
	Manipulations m=new Manipulations();
	
	@POST
	public void addNumber(int number) throws Exception{
		if(number<1||number>100){
			throw new Exception("Invalid Number");
		}
		else{
			m.setNumber(number);
		}
		
	}
	
	
	@GET
	public void displayDigits(){
		int s=m.getNumber();
		for(int i=1;i<=s;i++){
			System.out.println(i);
		}
		
	}
	@GET
	public String divNumber(int number){
		return m.divNumber(number);
		
	}
	
	
	
	
	
}
